#!/bin/sh
# translate .lp to .gms files
# used for new instances added when doing 1st revision of QPLIB paper

set -e

rm -f clean.lp

for f in *.lp
do

gms=${f/.lp/.gms}

[ -e $gms ] && continue

echo "$f -> $gms"

# replace " ^ 2" by "^2"
# replace # by _
# replace _er by er for QPLIB_10025 .. QPLIB_10029
# replace _svar by svar
# replace _scon by scon
# replace _sobj by sobj
# replace card by cardequ
# append line number to c equations for duplicates in T-Ramp, netgen, and radar instances
sed -e 's/ \^ 2/^2/g' -e 's/#/_/g' -e 's/_er/er/g' -e 's/_svar/svar/g' -e 's/_scon/scon/g' -e 's/_sobj/sobj/g' -e 's/card/cardequ/g' $f \
| awk '
/c[0-9]*:/ { gsub(":", sprintf("_%d:", NR), $1); }
{ print; }' \
> clean.lp

~/work/zib/scip/bin/scip -c "read clean.lp write problem clean.gms quit" > /dev/null

# bring back objective
if grep -wq objvar clean.gms && grep -q quadobj clean.gms ; then
echo "Glue quadobj into obj"
awk '
BEGIN { inquadobj = 0; inobjequ = 0; }
/ \.\./ { inobjequ = 0; }
/objequ \.\./ { inobjequ = 1; }
/, quadobjvar/ { gsub(", quadobjvar",""); }
/quadobj, / { gsub("quadobj, ",""); }
inobjequ && /+quadobjvar/ { gsub("+quadobjvar", ""); }
inobjequ && /quadobjvar/ { gsub("quadobjvar", ""); }
#inobjequ && /quadobjvar / { gsub("(quadobjvar ", "("); }
inobjequ { gsub(");", ""); }
/quadobj \.\./  { $0 = substr($0, 26); inquadobj=1; }
/=l= 0/ { if( inquadobj ) { gsub("=l= 0", ")"); inquadobj = 0; } }
{ print; }
' clean.gms > clean2.gms

elif grep -q quadobj clean.gms ; then
echo "Make quadobj an =E= obj"
awk '
BEGIN { inquadobj = 0; }
/quadobj ../  {  inquadobj=1; }
/=l= 0/ { if( inquadobj ) { gsub("=l=", "=e="); inquadobj = 0; } }
{ print; }
' clean.gms > clean2.gms

else
cp clean.gms clean2.gms
fi
#echo "quadobj remains: $(grep -c quadobj clean2.gms)"

# remove xdummy
#grep xdummy clean2.gms
sed -e "/xdummy.fx/d" -e "s/xdummy,//" -e "s/, xdummy;/;/" -e "s/xdummy/0/" clean2.gms > $gms
#grep xdummy $gms

done

rm -f clean.lp clean.gms clean2.gms
