#!/bin/sh
# runs .gms file through gms2gms and add result to qplib data/gms
# used for new instances added when doing 1st revision of QPLIB paper

set -e

rm -f gms2gms*

for f in *.gms
do

lp=${f/.gms/.lp}

res=../../data/gms/${f^^[qplib]}

[ -e $res ] && continue

#rm $f
#./lp2gms.sh

echo $f " -> " $res

../../scripts/gms2gms.py $f $res

# write info file with SOURCE
meta=../../data/meta/${f^^[qplib]}
meta=${meta/.gms/.info}
awk '/Problem name/ { printf("SOURCE = %s\n", $3); quit; }' $lp > $meta

done
